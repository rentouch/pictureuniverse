from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.scatter import Scatter
from kivy.properties import StringProperty, BooleanProperty, ListProperty
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.base import EventLoop
from kivy.uix.image import Image
from kivy.factory import Factory
from kivy.uix.widget import Widget
from kivy.animation import Animation
from kivy.uix.button import Button

from glob import glob
from os.path import join, dirname
from functools import partial
import os
import json

from PB.Act import PBAct
from filecrawler import FileCrawler


#if pbact is activated in pbase settings active it
use_pbact = False
path = os.path.expanduser('~/apps/1/settings.json')
if os.path.isfile(path):
    f=open(path)
    json_data = json.load(f)
    f.close()
    #check key
    try:
        for setting in json_data:
            try:
                if setting['key']=='pbact':
                    use_pbact = setting['value']
            except:
                pass
    except:
        pass
if use_pbact:
    PBAct.start(softwareid=5)
    

class MainLayout(FloatLayout):
    images = ListProperty([]) #containing all picture instances
    active_scene = "NONE"
    run_usb_animation = True
    
    def __init__(self, **kwargs):
        super(MainLayout, self).__init__(**kwargs)
        #####Segfault quick-fix####
        #### We got some issues with loading the images and a carousel instance 
        #### This leaded to a problem with kivy.atlas.
        #### We fix this by creating a button instance first. The button us using the atlas 
        self.fix_button = Button()
        ####
        
        #get different scenes on which the pictures are getting displayed
        self.free_scene = Factory.FreeScene()
        self.fullscreen_scene = Factory.FullscreenScene()
        
        #set default scene
        self.show_free()
        
        #load local images from HD
        #self.load_local_images()
        
        #pbact callback
        PBAct.on_file_insert = self.load_pbact_image
        
        #Call usb-stick file-crawler with desired callbacks
        self.fc = FileCrawler(on_found_file=self.load_image, 
                              on_lost_file=self.unload_local_image,
                              on_found_media=self.found_media,
                              on_lost_media=self.lost_media)
        
        #start
        self.fc.start()
        
        #start usb drive animation
        self.usb_animation()
    
    
    def found_media(self, *kwargs):
        print "found media"
        self.loader_label.text = "Searching images on USB-drive, please wait..."
        if self.usb_stick in self.loader_scene.children:
            self.loader_scene.remove_widget(self.usb_stick)
    
    
    def lost_media(self, *kwargs):
        print 'lost media'
        if self.loader_scene not in self.children:
            self.add_widget(self.loader_scene)
        self.loader_label.text = "Please insert your USB-drive..."
        if self.usb_stick not in self.loader_scene.children:
            self.loader_scene.add_widget(self.usb_stick)
            
            
    def usb_animation(self, *kwargs):
        '''Get called whenever the animation (fade) of the usb stick finished
        It starts a new fade animation.
        '''
        if self.run_usb_animation:
            if self.usb_stick.color[3]==0.2:
                ani=Animation(color=(1,1,1,0.8), d=2, t='in_out_cubic')
                ani.start(self.usb_stick)
                ani.bind(on_complete=self.usb_animation)
            else:
                ani=Animation(color=(1,1,1,0.2), d=2, t='in_out_cubic')
                ani.start(self.usb_stick)
                ani.bind(on_complete=self.usb_animation)
            
   
    def load_pbact_image(self, pbfile, *kwargs):
        '''Get called whenever a new file gets added to the session
        We will check for image files and display them afterwards
        '''
        print "*****______AADDED_____:", pbfile.filename
        
        #create image an add it to scene
        if pbfile.filename.split(".")[-1] in ('jpg', 'png'):
            try:
                #load the image
                pbfile.widget = PictureImage(source='icon.png', pbfile = pbfile)
                pbfile.bind(pbfile.widget.setter("source"))
                pbfile.on_delete = self.unload_pbact_image
                
                #append it to the images list
                self.images.append(pbfile.widget)
                
                #add it to a scene
                if self.active_scene == 'free':
                    self.show_free_single(pbfile.widget)
                elif self.active_scene == 'fullscreen':
                    self.show_fullscreen_single(pbfile.widget)  
            except:
                print "error while loading image from pbact"
    
    
    def unload_pbact_image(self, pbfile, *kwargs):
        for image in self.images:
            if pbfile.widget == image:
                self.remove_image(image)
    
        
    def load_local_images(self):
        '''Loads the images from /pictures
        '''
        filenames=[]
        path = os.path.expanduser('~/apps/29/settings.json')
        for filename in glob(join(path, 'pictures', '*')):
            if len(filename.split("."))  >= 2:
                filenames.append(filename)
        Clock.schedule_once(partial(self.load_image, filenames), 0)
        
    
    def unload_local_image(self, path, *kwargs):
        '''Removes a local image (given by path)
        '''
        for image in self.images:
            if image.source==path:
                self.remove_image(image)
                break
    
    
    def load_image(self, filenames, *kwargs):
        '''Loads one image and place it
        '''
        if self.loader_scene not in self.children:
            self.add_widget(self.loader_scene)
        self.loader_label.text = "Loading images from the USB-drive, please wait..."
            
        if len(self.images) > 25:
            print "too much images already on stage!"
            if self.loader_scene in self.children:
                self.remove_widget(self.loader_scene)
            return
        #fetch first filename in filenames list
        filename=filenames[0]
        
        #try load the image
        try:
            picture = PictureImage(source=filename, pbfile=None)
        except:
            print "Can't load image with source:", filename
            filenames.pop(0)
            #load next image, if there is a next one
            if filenames:
                Clock.schedule_once(partial(self.load_image, filenames), 0.2)
                return
            #when no images are loaded yet and there isn't a next one to load
            if not self.images:
                self.loader_label.text = "Only invalid image(s) found on usb drive..."
            return
        
        #append it to the images list
        self.images.append(picture)
        
        #add it to a scene
        if self.active_scene == 'free':
            self.show_free_single(picture)
        elif self.active_scene == 'fullscreen':
            self.show_fullscreen_single(picture)     
               
        #remove this filename from the filenames list
        filenames.pop(0)
        
        #load next image or remove load scene
        if filenames:
            Clock.schedule_once(partial(self.load_image, filenames), 0.2)
        else:
            if self.loader_scene in self.children:
                self.remove_widget(self.loader_scene)
            
    
    def remove_image(self, image, *kwargs):
        '''Removes the desired image
        '''
        self.remove_free_single(image)
        self.remove_fullscreen_single(image)
        self.images.remove(image)
    
    
    def clear_images(self):
        '''Remove all children / sub-children from all scenes
        '''
        for scatter_picture in self.free_scene.children:
            scatter_picture.clear_widgets()
        self.free_scene.clear_widgets()
        try:
            self.fullscreen_scene.clear_widgets()
        except:
            pass
       
    
    def show_free(self):
        '''Show the pictures in scatter mode
        '''
        if self.active_scene == "free":
            return
        self.clear_images()
        self.scene.clear_widgets()
        self.scene.add_widget(self.free_scene)
        self.active_scene = "free"
        for image in self.images:
            self.show_free_single(image)
            
            
    def show_free_single(self, image):
        '''Load one picture in scatter mode
        '''
        if self.active_scene == "free":
            image.size = 400, 400/image.image_ratio
            picture = ScatterPicture(center=Window.center, size=image.size,
                                     rotation=len(self.free_scene.children)*-5)
            picture.add_widget(image)
            self.free_scene.add_widget(picture)
            
    
    def remove_free_single(self, image, *kwargs):
        '''Remove one picture in scatter mode
        '''
        if self.active_scene == "free":
            for img in self.images:
                if img == image:
                    img.parent.parent.remove_widget(img.parent)
                    return
    
    
    def show_fullscreen(self):
        '''Show the pictures in fullscreen mode
        '''
        if self.active_scene == "fullscreen":
            return
        self.clear_images()
        self.scene.clear_widgets()
        self.scene.add_widget(self.fullscreen_scene)
        self.active_scene = "fullscreen"
        for image in self.images:
            self.show_fullscreen_single(image)
        
        
    def show_fullscreen_single(self, image):
        '''Load one picture in fullscreen mode
        '''
        if self.active_scene == "fullscreen":
            self.fullscreen_scene.add_widget(image)
    
    
    def remove_fullscreen_single(self, image, *kwargs):
        '''Removes an image in fullscreen mode
        '''
        if self.active_scene == "fullscreen":
            for img in self.images:
                if img == image:
                    self.fullscreen_scene.remove_widget(img)

        
        
class PictureImage(Image):
    
    def __init__(self, pbfile, **kwargs):
        super(PictureImage, self).__init__(**kwargs)
        self.pbfile = pbfile
        if not pbfile:
            self.remove_widget(self.delete_button)
    
    
    def on_texture_size(self, *kwagrs):
        try:
            self.parent.size = self.size
        except:
            pass



class ScatterPicture(Scatter):
    def __init__(self, **kwargs):
        super(ScatterPicture, self).__init__(**kwargs)
 
        

class TouchBlocker(Widget):
    '''All touches won't be forwarded to a next widget
    '''
    def __init__(self, **kwargs):
        super(TouchBlocker, self).__init__(**kwargs)
    def on_touch_down(self, touch):
        return True
    def on_touch_up(self, touch):
        return True



class PictureUniverseApp(App):
    
    def build(self):
        ml = MainLayout()
        EventLoop.bind(on_stop=ml.fc.stop)
        return ml
        
        
if __name__ == '__main__':
    PictureUniverseApp().run()
    