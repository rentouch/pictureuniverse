#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#===============================================================================
# Written by Rentouch GmbH 2013 - http://www.rentouch.ch
#===============================================================================

from kivy.clock import Clock
from threading import Thread
from functools import partial
from kivy.utils import platform

import time
import os


class FileCrawler():
    
    def __init__(self, on_found_file, on_lost_file, on_found_media, on_lost_media, **kwargs):
        self.on_found_file = on_found_file
        self.on_lost_file = on_lost_file
        self.on_found_media = on_found_media
        self.on_lost_media = on_lost_media
        self.working = False
        self.filetypes = ('jpg', 'png')
        self.files = []

        #set path in which dives get mounted
        if platform() == "macosx":
            self.path = "/Users/dominiqueburnand/Desktop/images"
        else:
            self.path = "/media"

        # Folders in the self.path dir which should not be searched for files.
        # This is needed until we can remove usbmount
        self.exclude_folders = ("usb", "usb0", "usb1", "usb2", "usb3", "usb4", "usb5", "usb6", "usb7")
    
    def start(self):
        """Start crawler
        """
        self.working = True
        
        #start crawler thread
        t = Thread(target=self.search_medias)
        t.start()
    
    def stop(self, *kwargs):
        """Stop crawler
        """
        print "stop crawler"
        self.working = False

    def search_medias(self, *kwargs):
        """ This function searches the folder media for subfolders
        the /media folder lists any plugged in devices
        """
        while self.working:
                
            #check if path is available
            if not os.path.isdir(self.path):
                print "The folder "+self.path+"can not be found. Wrong OS"
                print "Stop search for medias (usb devices) in crawler"
                self.stop()
                return
            
            #Check if any media is plugged in
            empty = True
            for r, dirs, f in os.walk(self.path):
                dirs[:] = [d for d in dirs if d not in self.exclude_folders]
                for files in f:
                    if not files.startswith("."):
                        empty = False
                    
            #if we did find the media:
            if not empty:
                #if we found some <filetype> files
                Clock.schedule_once(self.on_found_media)
                self.search_filetypes()
                break
                
            time.sleep(1)

    def search_filetypes(self):
        """Searches for any filetypes in media.
        If found at minnimum one file matching the filetype: list_files will be called
        """
        while self.working:
            empty = True
            drive_found = False
            for r, dirs, f in os.walk(self.path):
                dirs[:] = [d for d in dirs if d not in self.exclude_folders]
                for files in f:
                    if not files.startswith("."):
                        drive_found = True
                        if files.endswith(self.filetypes):
                            empty = False
            
            #if we found some files matching <filetypes>
            if not empty:
                self.list_files()
                break
            
            #if the media got lost
            if not drive_found:
                Clock.schedule_once(self.on_lost_media)
                self.search_medias()
                break
            
            time.sleep(1)

    def list_files(self, *kwargs):
        """Search for all files in media. Calls found_new_files or lost_files.
        """
        while self.working:
            #search for files
            all_files = []
            new_files = []
            old_files = []
            for r, dirs, f in os.walk(self.path):
                dirs[:] = [d for d in dirs if d not in self.exclude_folders]
                for files in f:
                    if files.endswith(self.filetypes) and not files.startswith("."):
                        filepath = os.path.join(r,files)
                        all_files.append(filepath)
                        if filepath not in self.files:
                            self.files.append(filepath)
                            new_files.append(filepath)
            if new_files:
                Clock.schedule_once(partial(self.on_found_file, new_files))
                        
            #check for files that got lost                
            for having_file in self.files:
                if not having_file in all_files:
                    self.files.remove(having_file)
                    old_files.append(having_file)
            for ofile in old_files:
                Clock.schedule_once(partial(self.on_lost_file, ofile))
                    
            #if no file is in the list, search for media
            if len(self.files) == 0:
                self.search_filetypes()
                break
            
            if not old_files:
                time.sleep(2.5)
